/**
 * 1.不要在开发环境下使用[chunkhash]，因为这会增加编译时间。
 * 将开发和生产模式的配置分开，并在开发模式中使用[name].js的文件名，
 *  在生产模式中使用[name].[chunkhash].js文件名。
 * 2.调试错误时使用webpack，不要使用watch监听，有些错误看不到。
 * 3.利用require.ensure()实现按需加载,终于找了好久终于在网上看到有人说还需要配置chunkFilename,
 * 和publicPath,好吧去看这俩的文档解释，才发现在介绍publicPath时提到了按需加载，并且说的不是很直接
 * ，意思就是按需加载单独打包出来的chunk是以publicPath会基准来存放的。好吧，另外还要配置
 * chunkFilename:[name].js这样才会最终生成正确的路径和名字
 */

const webpack = require('webpack'),
    path = require("path"),
    ROOT = path.resolve(__dirname, "../"),
    context = ROOT,
    autoprefixer = require('autoprefixer'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    postcssImport = require('postcss-import'),
    pageArr = require("./webpack.page.config"), //页面配置文件
    isProduction = process.env.ENV !== "DEV"; //是否是生产模式

//入口文件路径
const clientEntryPath = ROOT + "/src/";

var config = {
    entry: {
        //client入口
        "common": ["jquery", "reset-css", "tool-css", "common-js"],
        "common-css": ["common-css"],
        "index": clientEntryPath + "home/index.js",
        "aboutUs": clientEntryPath + "aboutUs/aboutUs.js",
        "news": clientEntryPath + "news/news.js",
        "cases": clientEntryPath + "case/cases.js",
        "service": clientEntryPath + "service/service.js",
        "contactUs": clientEntryPath + "contactUs/contactUs.js",
        "cane": clientEntryPath + "service/cane.js",
        "eBusiness": clientEntryPath + "service/eBusiness.js",
        "details": clientEntryPath + "news/details.js",
        //第三方
        "unslider": ["unslider-dot-css", "unslider-css", "unslider-js"],
        "respon-nav": ["respon-nav-css", "respon-nav-js"],
        "gridLayout": ["gridLayout-css"],
        "hamburgers": ["hamburgers-css"]
    },
    output: {
        filename: isProduction ? "js/[name].js?v=[hash:6]" : "js/[name].js",
        chunkFilename: "js/[name]chunk.js",
        path: ROOT + "/public/",
        publicPath: "/public/"
    },
    devtool: isProduction ? false : "source-map",
    module: {
        rules: [
            //{
            //         test: require.resolve("jquery"),
            //         use: "expose-loader?$"//expose-loader，这个loader的作用是，将指定js模块export的变量声明为全局变量
            //     },
            //     {
            //         test: require.resolve("jquery"),
            //         use: "expose-loader?jQuery"
            //     },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [{
                        loader: "css-loader",
                        options: {
                            minimize: isProduction,
                        }
                    }, {
                        loader: 'postcss-loader',
                        options: {
                            plugins: function () {
                                //解决scss中import其它sass文件不添加前缀的问题 或者在js入口文件中require
                                //https://github.com/postcss/postcss-loader/issues/35
                                return [
                                    postcssImport({
                                        addDependencyTo: webpack
                                    }),
                                    // return isProduction ? [autoprefixer] : [];                                    
                                    autoprefixer({
                                        browsers: ['last 1 Chrome versions', 'Android >= 4.0', 'iOS 7', 'ie 8'],
                                        cascade: true, //是否美化属性值 默认：true 像这样：
                                        remove: true //是否去掉不必要的前缀 默认：true 
                                    })
                                ];
                            }
                        }
                    }]
                })
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    allChunks: true,
                    use: [{
                            loader: "css-loader",
                            options: {
                                minimize: isProduction,
                                sourceMap: true //这个设为true没有关系，只有dvtool为true才起作用
                            }
                        },
                        {
                            loader: "sass-loader",
                            options: {

                            }

                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: function () {
                                    return [
                                        //解决scss中import其它sass文件不添加前缀的问题 或者在js入口文件中require
                                        //https://github.com/postcss/postcss-loader/issues/35
                                        postcssImport({
                                            addDependencyTo: webpack
                                        }),
                                        // return isProduction ? [autoprefixer] : [];
                                        autoprefixer({
                                            browsers: ['last 1 Chrome versions', 'Android >= 4.0', 'iOS 7', 'ie >=8'],
                                            cascade: true, //是否美化属性值 默认：true 像这样：
                                            remove: true //是否去掉不必要的前缀 默认：true 
                                        })
                                    ];
                                }
                            }
                        }
                    ]
                })
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: ['url-loader?limit=10240&name=[hash:6].[ext]&publicPath=../../public/&outputPath=images/']
            },
            {
                test: /\.(svg|woff|woff2|ttf|eot)$/,
                use: "file-loader?name=[hash:6].[ext]&publicPath=../&outputPath=fonts/"
            },
            {
                //处理html中的img
                test: /\.html$/,
                use: "html-loader"
                //use: "html-loader?interpolate"
            }
        ]
    },
    externals: {
        //这种方式一般需要在页面引入的js，并且把它的作用于扩展到全局
        // 我个人并不太看好这种做法，毕竟这就意味着jQuery脱离NPM的管理了，不过某些童鞋有其它的考虑，
        //例如为了加快每次打包的时间而把jQuery这些比较大的第三方库给分离出去（直接调用公共CDN的第三方库？），
        //也算是有一定的价值。
        //"BMap": "window.BMap"
    },
    resolve: {
        extensions: [".js", ".css"],
        alias: {
            "jquery": path.resolve(ROOT, "src/vendors/jquery/jquery-1.12.4.min.js"),
            "tool-css": path.resolve(ROOT, "src/common/tool.scss"),
            "common-css": path.resolve(ROOT, "src/common/common.scss"),
            "common-js": path.resolve(ROOT, "src/common/common.js"),
            "reset-css": path.resolve(ROOT, "src/vendors/reset/reset.css"), //样式重置
            "unslider-js": path.resolve(ROOT, "src/vendors/unslider/js/unslider-min.js"),
            "unslider-css": path.resolve(ROOT, "src/vendors/unslider/css/unslider.css"),
            "unslider-dot-css": path.resolve(ROOT, "src/vendors/unslider/css/unslider-dots.css"),
            "respon-nav-css": path.resolve(ROOT, "src/vendors/responsive-nav/responsive-nav.css"),
            "respon-nav-js": path.resolve(ROOT, "src/vendors/responsive-nav/responsive-nav.min.js"),
            "gridLayout-css": path.resolve(ROOT, "src/vendors/gridlayout/gridlayout.css"),
            "hamburgers-css": path.resolve(ROOT, "src/vendors/hamburget/hamburgers.css")
        }
    },
    plugins: [
        new webpack.DefinePlugin({
            'ENV': JSON.stringify(process.env.ENV)
        }),
        new webpack.ProvidePlugin({
            //兼容老式插件，凡是出现$,jQuery...的地方自动引入"jquery"这个模块
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            'window.$': 'jquery',
            responsiveNav: "respon-nav-js"
        }),
        //自动打包js
        new webpack.optimize.CommonsChunkPlugin({
            //names 将webpack的runtime单独提取
            names: ["common", "runtime"],
            filename: isProduction ? "js/[name].js?v=[hash:6]" : "js/[name].js",
            minChunks: Infinity //Infinity不会有其它js打包进来
        }),
        //提取css
        new ExtractTextPlugin(isProduction ? "css/[name].css?v=[hash:6]" : "css/[name].css"),
    ]
};

//DEV：开发环境
console.log("---------------目前webpack的环境为：" + (isProduction ? "生产环境" : "开发环境") + "-----");
console.log("---------------当前时间为：" + new Date().toLocaleTimeString() + "-----");
console.log("---------------当前根目录为：" + ROOT + "-----");
//是否开启压缩
if (isProduction) {
    config.plugins.push(new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false
        }
    }));
}
//页面page的配置
pageArr.forEach(function (value, index, array) {
    config.plugins.push(value);
});

module.exports = config;