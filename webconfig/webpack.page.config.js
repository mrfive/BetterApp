/**
 * webpack 页面配置
 * 在pageArr数组中添加对象
 */
const path = require("path"),
	ROOT = path.resolve(__dirname, "../"),
	clientFilenamePath = ROOT + "/app/views/client/",
	clientTemplatePath = ROOT + "/src/",
	HtmlWebpackPlugin = require('html-webpack-plugin');

var clientOption = {
	chunks: ["runtime", "common"]
};

//合并配置文件，返回新的数组
function concatChunks(arr) {
	return clientOption.chunks.concat(arr);
}

var pageArr = [
	//////////////////////////////////////////client页面///////////////////////////////////////////////

	//layout
	new HtmlWebpackPlugin({
		template: clientTemplatePath + "layout.html",
		filename: clientFilenamePath + "layout.html",
		chunks: ["runtime",  "common","respon-nav", "gridLayout", "hamburgers","common-css"], //在模版页注入css是因为code split 进来的css会插在head中，会先引入的。
		inject: true,
		chunksSortMode: 'manual'//按顺序来嘛~
		// chunksSortMode: function (chunk1, chunk2) {
		// 	var order = ["runtime", "common"];
		// 	var order1 = order.indexOf(chunk1.names[0]);
		// 	var order2 = order.indexOf(chunk2.names[0]);
		// 	return order1 - order2;
		// }
	}),
	//index
	new HtmlWebpackPlugin({
		template: clientTemplatePath + "home/index.html",
		filename: clientFilenamePath + "home/index.html",
		chunks: ["unslider", "index"],
		inject: true
	}),
	//aboutUs
	new HtmlWebpackPlugin({
		template: clientTemplatePath + "aboutUs/aboutUs.html",
		filename: clientFilenamePath + "aboutUs/aboutUs.html",
		chunks: ["aboutUs"],
		inject: true
	}),
	//news
	new HtmlWebpackPlugin({
		template: clientTemplatePath + "news/news.html",
		filename: clientFilenamePath + "news/news.html",
		chunks: ["news"],
		inject: true
	}),
	//cases
	new HtmlWebpackPlugin({
		template: clientTemplatePath + "case/cases.html",
		filename: clientFilenamePath + "case/cases.html",
		chunks: ["cases"],
		inject: true
	}),
	//service
	new HtmlWebpackPlugin({
		template: clientTemplatePath + "service/service.html",
		filename: clientFilenamePath + "service/service.html",
		chunks: ["service"],
		inject: true
	}),
	//contactUs
	new HtmlWebpackPlugin({
		template: clientTemplatePath + "contactUs/contactUs.html",
		filename: clientFilenamePath + "contactUs/contactUs.html",
		chunks: ["contactUs"],
		inject: true
	}),
	//cane
	new HtmlWebpackPlugin({
		template: clientTemplatePath + "service/cane.html",
		filename: clientFilenamePath + "service/cane.html",
		chunks: ["cane"],
		inject: true
	}),
	//eBusiness
	new HtmlWebpackPlugin({
		template: clientTemplatePath + "service/eBusiness.html",
		filename: clientFilenamePath + "service/eBusiness.html",
		chunks: ["eBusiness"],
		inject: true
	}),
	//details
	new HtmlWebpackPlugin({
		template: clientTemplatePath + "news/details.html",
		filename: clientFilenamePath + "news/details.html",
		chunks: ["details"],
		inject: true
	})
];

module.exports = pageArr;