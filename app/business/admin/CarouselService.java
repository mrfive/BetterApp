package business.admin;

import models.Carousel;
import models.Cases;
import play.Logger;
import play.Play;
import play.db.jpa.JPA;
import utils.DateFormat;

import javax.persistence.Query;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * Created by 伍冠源 on 2017/10/25.
 */
public class CarouselService {

    /**
     * 获取分页数据
     *
     * @param page
     * @param limit
     * @return
     */
    public static List<Carousel> getCarousel(int page, int limit) {
        try {
            final String hql = "select c from Carousel c order by c.number";
            Query query = JPA.em().createQuery(hql);
            query.setFirstResult((page - 1) * limit);//从第几条开始查询
            query.setMaxResults(limit);//最多取多少条
            return query.getResultList();
        } catch (Exception ex) {
            Logger.error("轮播图分页查询", ex);
        }

        return null;

    }

    /**
     * 获取所有的轮播图
     * 按编号排序
     *
     * @return
     */
    public static List<Carousel> getAllCarousel() {
        try {
            final String hql = "select c from Carousel c where c.isEnable=true order by c.number";
            Query query = JPA.em().createQuery(hql);
//            query.setParameter("isEnable", 1);
            return query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }


    // 判断文件夹是否存在
    static void judeDirExists(File file) {

        if (file.exists()) {
            if (file.isDirectory()) {
                System.out.println("目录已存在");
            } else {
                System.out.println("存在相同的名称文件，不能创建目录");
            }
        } else {
            System.out.println("文件夹不存在创建中...");
            file.mkdir();
            file.mkdirs();
        }

    }

    /**
     * 保存图片
     *
     * @param file
     * @return
     */
    public static File saveImg(String imgUrl, File file) throws IOException {

        String string = file.getName(); //获得文件名称
        long timeMillis = System.currentTimeMillis(); //获得时间(时间戳)

        //根据当前时间(时间戳)+文件的后缀组成一个字段用于数据库保存
        String alertPlayimg = timeMillis + (string.substring(string.lastIndexOf("."), string.length()));
        String imgFileStr = Play.applicationPath.toPath().resolve(imgUrl + "/" + alertPlayimg).toFile().getCanonicalPath();
        String dirStr = Play.applicationPath.toPath().resolve(imgUrl).toFile().getCanonicalPath();
        File imgFile = new File(imgFileStr);
        File dirFile = new File(dirStr);
        Path path = imgFile.toPath();

        FileInputStream in = null;

        try {
            judeDirExists(dirFile);
            in = new FileInputStream(file);
            Files.copy(in, path);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (in != null) in.close();
        }
        return new File(imgUrl + "/" + alertPlayimg);
    }

    /**
     * 编辑轮播图
     *
     * @param carousel
     * @return
     */
    public static boolean editCarousel(Carousel carousel) {
        try {
            Query query = JPA.em().createQuery("update Carousel c set c.number=:number,c.time=:time,c.imgUrl=:imgUrl,c.jumpUrl=:jumpUrl,c.isEnable=:isEnable,c.isEnableHref=:isEnableHref where c.id=:id");
            query.setParameter("number", carousel.number);
            query.setParameter("time", DateFormat.getNow());
            query.setParameter("imgUrl", carousel.imgUrl);
            query.setParameter("jumpUrl", carousel.jumpUrl);
            query.setParameter("isEnable", carousel.isEnable);
            query.setParameter("isEnableHref", carousel.isEnableHref);
            query.setParameter("id", carousel.id);
            return query.executeUpdate() > 0;
        } catch (Exception ex) {
            Logger.error("编辑轮播图", ex);
        }
        return false;
    }

    /**
     * 删除轮播图
     *
     * @param id
     * @return
     */
    public static boolean deleteCarousel(long id) {
        models.Carousel carousel = (Carousel) Carousel.findById(id);
        if (carousel != null) {
            carousel.delete();
            return true;
        }
        return false;
    }

}


