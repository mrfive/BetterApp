package business.admin;

import models.Article;
import play.db.jpa.JPA;
import utils.DateFormat;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by 伍冠源 on 2017/10/29.
 */
public class ArticleService {
    /**
     * 获取分页数据
     *
     * @param page
     * @param limit
     * @return
     */
    public static List<Article> getArticlePage(int page, int limit) {
        try {
            final String hql = "select a from Article a order by a.id";
            Query query = JPA.em().createQuery(hql);
            query.setFirstResult((page - 1) * limit);//从第几条开始查询
            query.setMaxResults(limit);//最多取多少条
            return query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    /**
     * 获取分页
     * @param type 文章类型
     * @param page 页数
     * @param limit 每页数
     * @return
     */
    public static List<Article> getArticlePage(int type, int page, int limit) {
        try {
            final String hql = "select a from Article a where a.type=:type order by a.id";
            Query query = JPA.em().createQuery(hql);
            query.setParameter("type",type);
            query.setFirstResult((page - 1) * limit);//从第几条开始查询
            query.setMaxResults(limit);//最多取多少条
            return query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    /**
     * 编辑文章
     *
     * @param article
     * @return
     */
    public static boolean editArticle(Article article) {
        try {
            Query query = JPA.em().createQuery("update Article c set c.imgUrl=:imgUrl,c.title=:title,c.type=:type,c.time=:time,c.content=:content,c.isEnable=:isEnable,c.abstracts=:abstracts where c.id=:id");
            query.setParameter("imgUrl", article.imgUrl);
            query.setParameter("time", DateFormat.getNow());
            query.setParameter("title", article.title);
            query.setParameter("type", article.type);
            query.setParameter("content", article.content);
            query.setParameter("isEnable", article.isEnable);
            query.setParameter("abstracts", article.abstracts);
            query.setParameter("id", article.id);
            return query.executeUpdate() > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    /**
     * 删除文章
     *
     * @param id
     * @return
     */
    public static boolean deleteArticle(long id) {
        Article c = (Article) Article.findById(id);
        if (c != null) {
            c.delete();
            return true;
        }
        return false;
    }


}
