package business.admin;

import models.Cases;
import play.db.jpa.JPA;
import utils.DateFormat;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by 伍冠源 on 2017-10-27.
 */
public class CaseService {
    /**
     * 获取分页数据
     *
     * @param page
     * @param limit
     * @return
     */
    public static List<Cases> getCasePage(int page, int limit) {
        try {
            final String hql = "select c from Cases c order by c.id";
            Query query = JPA.em().createQuery(hql);
            query.setFirstResult((page - 1) * limit);//从第几条开始查询
            query.setMaxResults(limit);//最多取多少条
            return query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;

    }

    /**
     * 获取所有客户案例
     *
     * @return
     */
    public static List<Cases> getAllCase() {
        try {
            final String hql = "select c from Cases c order by c.id";
            Query query = JPA.em().createQuery(hql);
            return query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;

    }

    /**
     * 首页获取客户案例
     *
     * @return
     */
    public static List<Cases> getFourCase() {
        try {
            final String hql = "select c from Cases c order by c.id";
            Query query = JPA.em().createQuery(hql);
            query.setMaxResults(4);
            return query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;

    }

    /**
     * 编辑客户案例
     *
     * @param cases
     * @return
     */
    public static boolean editCases(Cases cases) {
        try {
            Query query = JPA.em().createQuery("update Cases c set c.imgUrl=:imgUrl,c.name=:name,c.type=:type,c.time=:time,c.description=:description where c.id=:id");
            query.setParameter("imgUrl", cases.imgUrl);
            query.setParameter("time", DateFormat.getNow());
            query.setParameter("name", cases.name);
            query.setParameter("type", cases.type);
            query.setParameter("description", cases.description);
            query.setParameter("id", cases.id);
            return query.executeUpdate() > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    /**
     * 删除客户案例
     *
     * @param id
     * @return
     */
    public static boolean deleteCase(long id) {
        Cases c = (Cases)Cases.findById(id);
        if (c != null) {
            c.delete();
            return true;
        }
        return false;
    }
}
