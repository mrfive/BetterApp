package business.admin;

import models.User;
import play.db.jpa.JPA;

import javax.persistence.Query;

/**
 * Created by Edward on 2017-10-24.
 */
public class UserService {

    /**
     * 根据用户名获取User
     *
     * @param username 用户名
     * @return
     */
    public static User getUserByUsername(String username) {
        User user = User.find("byUsername", username).first();
        return user;
    }

    /**
     * 修改密码
     *
     * @param user
     * @return
     */
    public static boolean changePwd(User user) {
        try {
            Query query = JPA.em().createQuery("update User u set u.password=:password where u.id=:id");
            query.setParameter("password", user.password);
            query.setParameter("id", user.id);
            return query.executeUpdate() > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    /**
     * 是否锁定用户
     *
     * @param id
     * @param b
     * @return
     */
    public static boolean lockUser(long id, boolean b) {
        try {
            Query query = JPA.em().createQuery("update User u set u.isLock=:isLock where u.id=:id");
            query.setParameter("isLock", b);
            query.setParameter("id", id);
            return query.executeUpdate() > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    /**
     *设置登录错误次数
     * @param id
     * @param errorCount
     * @return
     */
    public static boolean setUserErrorCount(long id, int errorCount) {
        try {
            Query query = JPA.em().createQuery("update User u set u.errorCount=:errorCount where u.id=:id");
            query.setParameter("errorCount", errorCount);
            query.setParameter("id", id);
            return query.executeUpdate() > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
