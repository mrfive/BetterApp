package utils;

/**
 * Created by 伍冠源 on 2017-10-26.
 * 请求消息返回类
 */
public final class ResponseInfo {

    //ignore setter and getter
    private int code;
    private String msg;

    private ResponseInfo(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 成功的消息
     *
     * @param msg
     * @return
     */
    public static final ResponseInfo success(String msg) {
        return new ResponseInfo(0, msg);
    }

    /**
     * 失败的消息
     *
     * @param msg
     * @return
     */
    public static final ResponseInfo fail(String msg) {
        return new ResponseInfo(-1, msg);
    }

    /**
     * 其它的消息
     *
     * @param msg
     * @return
     */
    public static final ResponseInfo other(int code, String msg) {
        return new ResponseInfo(code, msg);
    }
}
