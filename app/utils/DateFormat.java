package utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by fuck on 2017/10/26.
 */
public class DateFormat {

    private final static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static String getNow() {
        return formatter.format(new Date());
    }
}
