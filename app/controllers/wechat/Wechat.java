package controllers.wechat;

import play.Play;
import play.mvc.Controller;

public class Wechat extends Controller {
    public static void index() {
        String code = params.get("code");
        String state = params.get("state");
        String wechatrl = Play.configuration.getProperty("wechatUrl");
        if (code != null && !"".equals(code) && !"".equals(wechatrl)) {
            String[] wechatUrls = wechatrl.split(",");
//            if ("1".equals(state)) {
//                redirect("http://unicorn.nnbetter.com:8082/?app=unicorn&code=" + code + "#/");
//            } else if ("2".equals(state)) {
//                redirect("http://wanji.nnbetter.com:8301/member.html?code=" + code);
//            } else if ("3".equals(state)) {
//                redirect("http://wanji.nnbetter.com:8301/wallet_recharge.html?code=" + code);
//            } else if ("4".equals(state)) {
//                redirect("http://wanji.nnbetter.com:8301/order_ok.html?code=" + code);
//            }


            redirect(wechatUrls[Integer.parseInt(state) - 1] + code);

        }
        String reUrl = params.get("reUrl");
        render(reUrl);

    }
}