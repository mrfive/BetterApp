package controllers.secure;

import controllers.admin.Login;
import play.Play;
import play.mvc.Before;
import play.mvc.Controller;

/**
 * Created by 伍冠源 on 2017-10-25.
 */
public class Secure extends Controller {

    //类方法执行前操作
    @Before(unless = {"admin.Login.login", "admin.Login.authenticate"})
    static void checkAccess() throws Throwable {
        //身份验证
        if (!session.contains("username")) {
            flash.put("url", "GET".equals(request.method) ? request.url : Play.ctxPath + "/");
            Login.login();
        }
    }

}


