package controllers.admin;

import business.admin.UserService;
import controllers.secure.Secure;
import models.User;
import play.Play;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.libs.Crypto;
import play.mvc.Controller;
import play.mvc.With;


/**
 * Created by 伍冠源 on 2017-10-19.
 */
@With(Secure.class)
public class Login extends Controller {

    static User connected() {
        if (renderArgs.get("user") != null) {
            return renderArgs.get("user", User.class);
        }
        String username = session.get("username");
        if (username != null) {
            return User.find("byUsername", username).first();
        }
        return null;
    }

    /**
     * 登录页面
     */
    public static void login() {
        flash.keep("url");
        render();
    }

    /**
     * 登录
     *
     * @param username 用户名
     * @param password 密码
     */
    public static void authenticate(@Required String username, String password) {
        User user = UserService.getUserByUsername(username);
        if (user == null) {
            authFail("用户名或者密码错误");//验证失败
        } else {
            renderArgs.put("user", user);//存储用户
        }

        if (user.errorCount >= 10) {
            authFail("该账号输入错误次数过多已被锁定，请联系管理员");
        }

        Boolean allowed = false;
        String signStr = Crypto.sign(username + password);

        allowed = signStr.equals(user.password);

        if (Validation.hasErrors() || !allowed) {
            user.errorCount++;
            UserService.setUserErrorCount(user.id, user.errorCount);
            authFail("用户名或者密码错误");
        }
        session.put("username", username);
        Index.index();
        redirectToOriginalURL();
    }

    /**
     * 跳转到login，并存到flash
     */
    private static void authFail(String errorMsg) {
        flash.keep("url");
        flash.error(errorMsg);
        params.flash();//把params参数存储到flash中
        login();
    }

    /**
     * 重定向到上一个url
     */
    static void redirectToOriginalURL() {
        String url = flash.get("url");
        if (url == null) {
            url = Play.ctxPath + "/";
        }
        redirect(url);
    }

    /**
     * 注销
     */
    public static void logout() {
        session.remove("username");
        Login.login();
    }

}
