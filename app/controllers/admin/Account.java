package controllers.admin;

import business.admin.UserService;
import controllers.secure.Secure;
import models.User;
import play.libs.Crypto;
import play.mvc.Controller;
import play.mvc.With;
import utils.ResponseInfo;

/**
 * Created by 伍冠源 on 2017-11-10.
 */
@With(Secure.class)
public class Account extends Controller {

    /**
     * 密码页面
     */
    public static void pwd() {
        render();
    }

    /**
     * 修改密码
     *
     * @param oldPwd
     * @param newPwd1
     * @param newPwd2
     */
    public static void changePwd(String oldPwd, String newPwd1, String newPwd2) {
        String username = session.get("username");
        if ("".equals(username) || username == null) {
            renderJSON(ResponseInfo.fail("请重新登录"));
        }
        if (!newPwd1.equals(newPwd2)) {
            renderJSON(ResponseInfo.fail("确认密码和密码不一致"));
        }

        User currentUser = UserService.getUserByUsername(username);
        String oldSign = Crypto.sign(username + oldPwd);

        if (!oldSign.equals(currentUser.password)) {
            renderJSON(ResponseInfo.fail("原密码不正确"));
        }

        String signStr = Crypto.sign(username + newPwd1);
        currentUser.password = signStr;

        if (UserService.changePwd(currentUser)) {
            renderJSON(ResponseInfo.success("修改密码成功"));
        }
        renderJSON(ResponseInfo.fail("修改密码失败"));
    }
}
