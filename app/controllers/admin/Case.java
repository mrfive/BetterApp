package controllers.admin;

import business.admin.CarouselService;
import business.admin.CaseService;
import controllers.secure.Secure;
import models.Cases;
import play.Logger;
import play.mvc.Controller;
import play.mvc.With;
import utils.DateFormat;
import utils.ResponseInfo;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 伍冠源 on 2017-10-27.
 * 客户案例
 */
@With(Secure.class)
public class Case extends Controller {

    final static String  imgUrl = "./publicImages/case";

    /**
     * 管理页面
     */
    public static void cases() {
        render();
    }

    /**
     * 分页
     */
    public static void getCasePage(int page, int limit) {
        List<Cases> cases = CaseService.getCasePage(page, limit);
        Map jsonMap = new HashMap();
        jsonMap.put("count", Cases.count());
        jsonMap.put("data", cases);
        jsonMap.put("code", 0);
        renderJSON(jsonMap);
    }

    /**
     * 添加客户案例
     */
    public static void addCase(Cases cases, File file) {
        if (file == null) {
            renderJSON(ResponseInfo.fail("请上传图片"));
        }


        try {
            File imgFile = CarouselService.saveImg(imgUrl, file);
            cases.imgUrl = imgFile.getPath().replaceAll("\\\\", "/").substring(1);
            cases.time = DateFormat.getNow();
            cases.save();
            renderJSON(ResponseInfo.success("添加客户案例成功"));
        } catch (IOException ex) {
            Logger.error("添加客户案例", ex);
        }

        renderJSON(ResponseInfo.fail("添加客户案例失败"));
    }

    /**
     * 编辑客户案例
     *
     * @param cases
     * @param file
     */
    public static void editCase(Cases cases, File file) {
        if (file != null) {
            try {
                File imgFile = CarouselService.saveImg(imgUrl, file);
                cases.imgUrl = imgFile.getPath().replaceAll("\\\\", "/").substring(1);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        if (CaseService.editCases(cases)) {
            renderJSON(ResponseInfo.success("编辑客户案例成功"));
        }

        renderJSON(ResponseInfo.fail("编辑客户案例失败"));
    }

    /**
     * 删除客户案例
     *
     * @param id
     */
    public static void deleteCase(long id) {
        if (CaseService.deleteCase(id)) {
            renderJSON(ResponseInfo.success("删除客户案例成功"));
        }
        renderJSON(ResponseInfo.fail("删除客户案例失败"));
    }
}
