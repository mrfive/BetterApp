package controllers.admin;

import business.admin.ArticleService;
import business.admin.CarouselService;
import com.google.gson.JsonObject;
import controllers.secure.Secure;
import javafx.application.Application;
import models.Article;
import play.Play;
import play.mvc.Controller;
import play.mvc.With;
import utils.DateFormat;
import utils.ResponseInfo;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 伍冠源 on 2017-10-29.
 * 客户案例
 */
@With(Secure.class)
public class News extends Controller {

    final static String imgUrl = "./publicImages/article";

    /**
     * 管理页面
     */
    public static void news() throws IOException {
        render();
    }

    /**
     * 发布文章
     *
     * @param article
     * @param file
     */
    public static void addArticle(Article article, File file) {
        if (file == null) {
            renderJSON(ResponseInfo.fail("请上传图片"));
        }

        try {
            File imgFile = CarouselService.saveImg(imgUrl, file);
            article.imgUrl = imgFile.getPath().replaceAll("\\\\", "/").substring(1);
            article.time = DateFormat.getNow();
            article.save();
            renderJSON(ResponseInfo.success("发布文章成功"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        renderJSON(ResponseInfo.fail("发布文章失败"));
    }

    /**
     * 新闻动态分页
     */
    public static void getCasePage(int page, int limit) {
        List<Article> list = ArticleService.getArticlePage(page, limit);
        Map jsonMap = new HashMap();
        jsonMap.put("count", Article.count());
        jsonMap.put("data", list);
        jsonMap.put("code", 0);
        renderJSON(jsonMap);
    }

    /**
     * 编辑文章
     *
     * @param article
     * @param file
     */
    public static void editArticle(Article article, File file) {
        if (file != null) {
            try {
                File imgFile = CarouselService.saveImg(imgUrl, file);
                article.imgUrl = imgFile.getPath().replaceAll("\\\\", "/").substring(1);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        if (ArticleService.editArticle(article)) {
            renderJSON(ResponseInfo.success("编辑文章成功"));
        }

        renderJSON(ResponseInfo.fail("编辑文章失败"));
    }

    /**
     * 删除文章
     *
     * @param id
     */
    public static void deleteArticle(long id) {
        if (ArticleService.deleteArticle(id)) {
            renderJSON(ResponseInfo.success("删除文章成功"));
        }
        renderJSON(ResponseInfo.fail("删除文章失败"));
    }

    /**
     * 上传图片（富文本编辑器用）
     *
     * @param file
     */
    public static void uploadImage(File file) {
        Map json = new HashMap();
        try {
            File imgFile = CarouselService.saveImg(imgUrl, file);
            List<String> urlList = new ArrayList<String>();
            String imgUrl = imgFile.getPath().replaceAll("\\\\", "/").substring(1);
            urlList.add(imgUrl);
            json.put("errno", 0);
            json.put("data", urlList);
            renderJSON(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
        json.put("errno", -1);
        renderJSON(json);
    }

}