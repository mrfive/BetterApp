package controllers.admin;

import business.admin.CarouselService;
import controllers.secure.Secure;
import models.Carousel;
import play.Logger;
import play.mvc.Controller;
import play.mvc.With;
import utils.DateFormat;
import utils.ResponseInfo;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 伍冠源 on 2017-10-25.
 */
@With(Secure.class)
public class Index extends Controller {

    final static String imgUrl = "./publicImages/carousel";

    /**
     * 首页页面
     */
    public static void index() {
        carousel();
    }

    /**
     * 轮播图管理
     */
    public static void carousel() {
        render();
    }


    /**
     * 添加轮播图
     */
    public static void addCarousel(Carousel carousel, File file) throws IOException {
        if (file == null) {
            renderJSON(ResponseInfo.fail("请上传图片"));
        }

        try {
            File imgFile = CarouselService.saveImg(imgUrl, file);
            carousel.imgUrl = imgFile.getPath().replaceAll("\\\\", "/").substring(1);

            carousel.time = DateFormat.getNow();
            carousel.save();
            renderJSON(ResponseInfo.success("添加轮播图成功"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        renderJSON(ResponseInfo.fail("添加轮播图失败"));


        //在本地项目的public下的images文件夹中保存源文件，名称就是时间戳格式
        //生成缩略图，即100*100的小图(用于显示小图)
//        Images.resize(file, Play.getFile("/public/images/" + alertPlayimg),100,100);

//如果是服务器，就自行修改路径就好
    }

    /**
     * 编辑轮播图片
     *
     * @param carousel
     * @param file
     */
    public static void editCarousel(Carousel carousel, File file) {
        if (file != null) {
            try {
                File imgFile = CarouselService.saveImg(imgUrl, file);
                carousel.imgUrl = imgFile.getPath().replaceAll("\\\\", "/").substring(1);
            } catch (IOException ex) {
                Logger.info("编辑轮播图片", ex);
            }
        }

        if (CarouselService.editCarousel(carousel)) {
            renderJSON(ResponseInfo.success("编辑轮播图成功"));
        }

        renderJSON(ResponseInfo.fail("编辑轮播图失败"));
    }

    /**
     * 轮播图分页
     */
    public static void getCarousel(int page, int limit) {
        List<Carousel> carousels = CarouselService.getCarousel(page, limit);
        Map jsonMap = new HashMap();
        jsonMap.put("code", 0);
        jsonMap.put("count", Carousel.count());
        jsonMap.put("data", carousels);
        jsonMap.put("msg", "查询成功");
        renderJSON(jsonMap);
    }

    /**
     * 删除轮播图
     *
     * @param id
     */
    public static void deleteCarousel(long id) {
        if (CarouselService.deleteCarousel(id)) {
            renderJSON(ResponseInfo.success("删除轮播图成功"));
        }
        renderJSON(ResponseInfo.fail("删除轮播图失败"));
    }
}
