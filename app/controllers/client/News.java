package controllers.client;

import business.admin.ArticleService;
import models.Article;
import play.mvc.Controller;

import java.util.List;

public class News extends Controller {
    /**
     * 公司动态
     */
    public static void news(int type, int page) {
        if (page <= 0) page = 1;
        if (type <= 0) type = 0;
        List<Article> articleList = ArticleService.getArticlePage(type, page, 6);
        long count = articleList.size() / 6 + 1;
        render(articleList, type, page, count);
    }


    /**
     * 文章详情
     */
    public static void details(long id) {
        if (id <= 0) id = 1;
        Article article = Article.findById(id);
        render(article);
    }


}