package controllers.client;

import business.admin.CarouselService;
import business.admin.CaseService;
import models.Carousel;
import models.Cases;
import play.mvc.Controller;

import java.util.List;

public class Home extends Controller {
    public static void index() {
        //轮播图
        List<Carousel> carouselList = CarouselService.getAllCarousel();
        //客户案例
        List<Cases> caseList = CaseService.getFourCase();
        render(carouselList, caseList);
    }
}