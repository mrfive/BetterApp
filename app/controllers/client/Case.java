package controllers.client;

import business.admin.CaseService;
import models.Cases;
import play.mvc.Controller;

import java.util.List;

public class Case extends Controller {
    public static void cases() {
        //客户案例
        List<Cases> caseList = CaseService.getAllCase();
        render(caseList);
    }
}