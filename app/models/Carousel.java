package models;

import play.data.validation.Required;
import play.db.jpa.Model;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 轮播图类
 * Created by 伍冠源 on 2017-10-25.
 */
@Entity
@Table(name = "carousel")
public class Carousel extends Model {

    //排序编号
    public short number;

    //添加时间
    public String time;

    //图片路径
    @Required
    public String imgUrl = "";

    //超链接路径
    public String jumpUrl;

    //是否启用
    public Boolean isEnable;

    //是否启用超链接
    public Boolean isEnableHref;

    public Carousel(short number, String time, String imgUrl, String jumpUrl, Boolean isEnable, Boolean isEnableHref) {
        this.number = number;
        this.time = time;
        this.imgUrl = imgUrl;
        this.jumpUrl = jumpUrl;
        this.isEnable = isEnable;
        this.isEnableHref = isEnableHref;
    }
}
