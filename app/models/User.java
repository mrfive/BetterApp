package models;

import org.hibernate.annotations.ColumnDefault;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.data.validation.Unique;
import play.db.jpa.Model;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 用户类
 * Created by 伍冠源 on 2017-10-24.
 */
@Entity
@Table(name = "user")
public class User extends Model {

    //用户名
    @Required
    @Unique
    //    @Column(columnDefinition="varchar(128) default 'hello' ") 默认值
    public String username;

    //密码
    @Required
    public String password;

    //是否锁定，禁止登录
    @ColumnDefault("false")//默认值
    public Boolean isLock;

    //登录错误次数
    public int errorCount; //int类型默认值就是0


    //姓名
    @Required
    @MaxSize(100)
    public String name;

    //用户类型
    public short type;

    public User(String username, String password, String name) {
        this.username = username;
        this.password = password;
        this.name = name;
    }

    public String toString() {
        return "User(" + username + ")";
    }
}
