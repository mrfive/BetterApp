package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * Created by 伍冠源 on 2017/10/29.
 */
@Entity
@Table(name = "article")
public class Article extends Model {

    //文章标题
    public String title;

    //文章缩略图
    public String imgUrl;

    //文章内容
    @Lob
    public String content;

    //文章类型 0:公司动态 1：行业聚焦 2：媒体报道
    public int type;

    //发布时间
    public String time;

    //是否启用
    public Boolean isEnable;

    //摘要
    public String abstracts;

    //发布者
//    public String author;
}
