package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by 伍冠源 on 2017-10-27.
 */
@Entity
@Table(name = "cases")
public class Cases extends Model {

    //名称
    public String name;

    //图片url
    public String imgUrl;

    //类型 0：微信公众号开发 1：APP开发 2：管理系统
    public short type;

    //添加时间
    public String time;

    //描述
    public String description;

    public Cases(String name, String imgUrl, short type, String time, String description) {
        this.name = name;
        this.imgUrl = imgUrl;
        this.type = type;
        this.time = time;
        this.description = description;
    }
}
