require("./index.scss");

/**
 * slider轮播
 * 
 */
function fnSlider() {
    var $imgs = $(".index-banner__img");
    $("#banner").unslider({
        delay: 3500,
        autoplay: true,
        arrows: false
    });
    $imgs.show();

    $imgs.click(function() {
        var url = $(this).data("url");
        if (url && $.trim(url) !== "") {
            window.location.href = url;
        }
    });
}

/**
 * 百度地图API功能
 * 
 */
function fnInitMap() {
    var map = new BMap.Map("allmap"), // 创建Map实例
        point = new BMap.Point(108.386901, 22.820769);
    map.centerAndZoom(point, 19); // 初始化地图,设置中心点坐标和地图级别
    map.setCurrentCity("南宁"); // 设置地图显示的城市 此项是必须设置的
    map.enableScrollWheelZoom(true); //开启鼠标滚轮缩放
    fnCreateMarker(map, point);

    function fnCreateMarker(map, point) {
        var marker = new BMap.Marker(point), // 创建标注
            label = new BMap.Label("南宁市贝特信息技术有限公司", {
                offset: new BMap.Size(20, -20)
            });
        map.addOverlay(marker); // 将标注添加到地图中
        marker.setAnimation(BMAP_ANIMATION_BOUNCE); //跳动的动画
        label.setStyle({
            color: "#1f93da",
            fontSize: "14px",
            borderColor: "#fff",
            fontFamily: '-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "PingFang SC", "Hiragino Sans GB", "Microsoft YaHei", sans-serif'
        });
        marker.setLabel(label);
    }


}


(function() {
    fnSlider();
    fnInitMap();
})();