/**
 * 全局常量
 */
var Constant = {
    SysError: "系统异常"
};

var Common = {
    /*
    提交表单
     */
    fnSubmitForm: function ($form, successMsg, failMsg, beforeSerialize) {

        beforeSerialize = beforeSerialize || function () {};
        try {
            +globalAddLayerIndex;
        } catch(err) {
            var globalAddLayerIndex = 0;
        }

        $form.ajaxForm({
            success: function (result, status, xhr, form) {
                layer.close(globalLoadIndex || 0);

                failMsg = result.msg || failMsg;
                successMsg = result.msg || successMsg;

                if (result && result.code >= 0) {
                    layer.close(globalAddLayerIndex);
                    layui.table.reload("carouselTable");
                    layer.msg(successMsg, {
                        icon: 1
                    });
                } else {
                    layer.msg(failMsg, {
                        icon: 2
                    });
                }
            },
            beforeSubmit: function () {
                globalLoadIndex = layer.load(1);
            },
            beforeSerialize: function () {
                beforeSerialize();
            },
            error: function () {
                layer.close(globalLoadIndex || 0);
                layer.msg(Constant.SysError, {
                    icon: 2
                });
            }
        });
    }

};

/**
 *初始化
 */
+
(function () {

    function fnPrevImg() {
        $(document).on("change", ".prevImgInput", function () {
            var docObj = $(this)[0],
                dd = $(this).parents(".prevImgInputBox").siblings(".preImgBox")[0];
            setImagePreviews(docObj, dd);
        });


        //多图片上传预览功能

        function setImagePreviews(docObj, dd) {
            dd.innerHTML = "";
            var fileList = docObj.files;
            for (var i = 0; i < fileList.length; i++) {
                dd.innerHTML += "<div> <img id='img" + i + "'  /> </div>";
                var imgObjPreview = $(dd).find("#img" + i)[0];
                if (docObj.files && docObj.files[i]) {
                    //火狐下，直接设img属性

                    imgObjPreview.className = "vendorImg am-img-responsive am-img-thumbnail";
                    imgObjPreview.style.display = 'block';
                    imgObjPreview.style.width = '185px';
                    imgObjPreview.style.height = 'auto';
                    //imgObjPreview.src = docObj.files[0].getAsDataURL();

                    //火狐7以上版本不能用上面的getAsDataURL()方式获取，需要一下方式

                    imgObjPreview.src = window.URL.createObjectURL(docObj.files[i]);
                } else {
                    //IE下，使用滤镜

                    docObj.select();
                    var imgSrc = document.selection.createRange().text;
                    var localImagId = $(dd).find("#img" + i)[0];
                    //必须设置初始大小

                    localImagId.style.width = "100%";
                    localImagId.style.height = "auto";
                    //图片异常的捕捉，防止用户修改后缀来伪造图片

                    try {
                        localImagId.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale)";
                        localImagId.filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = imgSrc;
                    } catch (e) {
                        toastr["warning"]("您上传的图片格式不正确，请重新选择!");
                        return false;
                    }
                    imgObjPreview.style.display = 'none';
                    document.selection.empty();
                }
            }
            return true;
        }
    }

    /**
     * 菜单显示
     */
    function fnShowMenu() {
        var url = window.location.href,
            $a = $("#menuTree .layui-nav-item").find("a");
        $a.each(function (index, ele) {
            var href = $(ele).attr("href");
            if ($.trim(href) !== "" && url.indexOf(href) !== -1) {
                $(ele).parent().addClass("layui-this");
                $(ele).parents(".layui-nav-item").addClass("layui-nav-itemed");
            }
        });
    }

    /**
     * 上传图片
     */
    function fnUploadImg() {
        $(document).on().on('change', ".prevImgInput", function () {
            var fileInput = this;
            lrz(this.files[0], {
                    // width: 640
                    quality: 1
                })
                .then(function (rst) {
                    fileInput.files[0] = rst.file;
                    // console.log("原图片的大小为:" + (rst.origin.size / 1024).toFixed(2) + "KB");
                    // console.log("生成后的图片的大小为:" + (rst.fileLen / 1024).toFixed(2) + "KB");
                    // console.log("生成后的base64图片的大小为:" + (rst.base64Len / 1024).toFixed(2) + "KB");
                })
                .catch(function (err) {
                    layer.msg("图片压缩异常", {
                        icon: 5,
                        shift: 6
                    });
                })
                .always(function () {

                });
        });
    }



    fnPrevImg();
    fnShowMenu();
    fnUploadImg();
})();